# Park Assist Dot Net Interview

Welcome! Here is your challenge. We expect it to take about four hours. Please ask questions.

## Problem 

Build a .NET application that 

 * Implements services which accept, aggregate and return data about a parking garage.
 * Has a background task which will clean out some data based on some given criteria.
 * (bonus) Presents that data visually.

## Deliverables

Please develop the following:

 * Services that accept data for:
     * bays
     * bay events
 * Application code to aggregate bay events into visits. A visit is a combination of an entry event and an exit event. See below for further details.
 * Services that present data about:
     * bays
     * bay events
     * visits
 * A continuously running background task which removes all completed visits that are less than 2 minutes long.
 * (bonus) A visual interface to see garage occupancy in real time.

## Details

You will have to simulate data for this project. Either a simple simulator that creates and updates bays and then transmits entry and exit events or some seed data will suffice.

### Bays

A **bay** is one parking space in a garage. A bay payload should look as follows: 

    { id: integer, occupied: boolean, out_of_service: boolean }

### Bay Events 

A **bay event** happens when a vehicle parks or leaves a space. Bay events are sent in order of occurrence. *Entry events* have type set to 'entry' and *exit events* have type set to 'exit'. A bay event payload should look as follows:

    { bay_id: integer, timestamp: datetime, type: string }

### Visits

Entry and exit bay events should be appropriately matched to become **visits**. `Dwell` is the difference between the entry time and exit time (or current time if none) in seconds. Please make your visits look like the following:
    
    { bay_id: integer, entry_timestamp: datetime, exit_timestamp: datetime, dwell: integer }